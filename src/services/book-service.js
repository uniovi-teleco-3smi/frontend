import Service from './service.js';

const resource = 'books';

export default {
    getAll() {
        return Service.get(resource);
    }
}
